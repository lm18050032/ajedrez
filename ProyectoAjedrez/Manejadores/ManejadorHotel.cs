﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesAjedrez;
using AccesoDatosAjedrez;

namespace Manejadores
{
   public class ManejadorHotel
    {
        Hotel eh = new Hotel();
        HotelAccesoDatos ah = new HotelAccesoDatos();
        public void InsertarMotel(Hotel dato)
        {
            ah.InsertarHotel(dato);
        }
        public void ActualizarMotel(Hotel dato)
        {
            ah.ActualizarHotel(dato);
        }
        public void EliminarMotel(int dato)
        {
            ah.EliminarHotel(dato);
        }
        public List<Hotel> MostrarMotel(string consulta)
        {
            var listObten = ah.ObtenerHoteles(consulta);
            return listObten;
        }
        public List<HotelExtrId> ExtraerIdHotel()
        {
            var listPais = ah.ExtraerIdHotel();
            return listPais;
        }
    }
}
