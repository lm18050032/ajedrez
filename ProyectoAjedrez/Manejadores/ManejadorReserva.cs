﻿using System;
using System.Collections.Generic;
using AccesoDatosAjedrez;
using EntidadesAjedrez;

namespace Manejadores
{
    
   public class ManejadorReserva
    {
        ReservacionesAccesoDatos rs = new ReservacionesAccesoDatos();
        Reserva r = new Reserva();
        
     public  void GuardarReservacion(Reserva dato) 
        {
            rs.InsertarReservaciones(dato);
        }
     public void ActualizarReservacion(Reserva dato)
    {
            rs.ActualizarReservacion(dato);
    }
    public void ELiminarReserva(int dato)
    {
            rs.EliminarReservacion(dato);
    }
    public List<Reserva> MostrarReservaciones(string consulta)
    {
            var listaReservaciones = rs.ObtenerReservas(consulta);
            return listaReservaciones;
    }
}
}
