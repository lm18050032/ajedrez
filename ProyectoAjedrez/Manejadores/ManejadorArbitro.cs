﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesAjedrez;
using AccesoDatosAjedrez;

namespace Manejadores
{
    public class ManejadorArbitro
    {
        ArbitrosAccesoDatos aa = new ArbitrosAccesoDatos();
        Arbitro ea = new Arbitro();
        public void GuardarArbitro(Arbitro dato)
        {
            aa.InsertarArbitro(dato);
        }
        /*
        public void GuardarArbitro(Arbitro dato)
        {
            aa.InsertarArbitro(dato);
        }
        */
        public void EliminarArbitro(string dato)
        {
            aa.EliminarArbitro(dato);
        }
        public List<VDatosArbitro> ObtenerArbitro(string consulta)
        {
            var listjugador = aa.ObtenerArbitro(consulta);
            return listjugador;
        }
        public List<ArbitroExtrid> ExtraerIdArbitro()
        {
            var listPais = aa.ObtenerIdArbitros();
            return listPais;
        }
    }
}
