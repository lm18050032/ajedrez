﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesAjedrez;
using AccesoDatosAjedrez;

namespace Manejadores
{
   public class ManejadorPartidas
    {
        Partidas ep = new Partidas();
        PartidasAccesoDatos ap = new PartidasAccesoDatos();
        JugadorExtId j = new JugadorExtId();
        public void IngresarPartida(Partidas dato)
        {
            ap.InsertarPartida(dato);
        }
        public void ModificarPartida(Partidas dato)
        {
            ap.ActualizarPartida(dato);
        }
        public void EliminarPartida(string dato)
        {
            ap.EliminarPartida(dato);
        }
        public List<VDatosPartidas> MostrarPartidas(string consulta)
        {
            var listObtenerPartida = ap.ObtenerPartidas(consulta);
            return listObtenerPartida;
        }
        // Pendiente Jugador1 y Jugador2 ComboBox
        public List<ParticipantesExtId> EstraeridJugador()
        {
            var listJug = ap.ObtenerIdJug();
            return listJug;
        }
        public List<ParticipantesExtId> ExtraerIdArb()
        {
            var listPart = ap.ObtenerIdArb();
            return listPart;
        }
        public List<HotelExtrId> ExtraerIdHOT()
        {
            var listHot = ap.ObtenerIdHot();
            return listHot;
        }
        public List<Sala> ExtraerIdSAL()
        {
            var listSal = ap.ObtenerIdSal();
            return listSal;
        }
    }
}
