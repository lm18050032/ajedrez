﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatosAjedrez;
using EntidadesAjedrez;

namespace Manejadores
{
   public class ManejadorMovimiento
    {
        MovimientoAccesoDatos am = new MovimientoAccesoDatos();
        Movimiento em = new Movimiento();
        public void InsertarMovimiento(Movimiento dato)
        {
            am.InsertarMovimientos(dato);
        }
        public void ActualizarMovimiento(Movimiento dato)
        {
            am.ActualizarMovimiento(dato);
        }
        public void EliminarMovimiento(Movimiento dato)
        {
            am.EliminarMovimiento(dato.IdMovimiento.ToString());
        }
        public List<Movimiento> MostrarMovimiento(string dato)
        {
            var listMovimiento = am.Obtenermovimientos(dato);
            return listMovimiento;
        }
        public List<Partidas> ExtraerIdPartida()
        {
            var listPart = am.ObtenerIdPartida();
            return listPart;
        }
    }
}
