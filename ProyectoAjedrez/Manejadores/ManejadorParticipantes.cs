﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatosAjedrez;
using EntidadesAjedrez;

namespace Manejadores
{
    public class ManejadorParticipantes
    {
        ParticipantesAccesoDatos pa = new ParticipantesAccesoDatos();
        Participantes ep = new Participantes();
        public void InsertarParticipante(Participantes dato)
        {
            pa.InsertarParticipantes(dato);
        }
        public void ActualizarParticipante(Participantes dato)
        {
            pa.ActualizarParticipante(dato);
        }
        public void EliminarParticipante(string dato)
        {
            pa.EliminarParticipante(dato);
        }
        public List<Participantes> MostrarParticipantes(string consulta)
        {
            var listParticipante = pa.ObtenerParticipantes(consulta);
            return listParticipante;
        }
        public List<ParticipantesExtId> ExtraerIdParticipante()
        {
            var listParticipantes = pa.ObtenerIdParticip();
            return listParticipantes;
        }
    }
}
