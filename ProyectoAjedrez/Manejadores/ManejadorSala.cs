﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesAjedrez;
using AccesoDatosAjedrez;
namespace Manejadores
{
    public class ManejadorSala
    {
        Sala s = new Sala();
        SalasAccesoDatos ad = new SalasAccesoDatos();




        public void InsertarSala(Sala dato)
        {
            ad.InsertarSalas(dato);
        }
        public void ActualizarSala(Sala dato)
        {
            ad.ActualizarSala(dato);
        }
        public void ELiminarSala(int dato)
        {
            ad.EliminarSala(dato);
        }
        public List<Sala> MostrarSala(string consulta)
        {
            var listsala = ad.ObtenerSalas(consulta);
            return listsala;
        }
    }
}
