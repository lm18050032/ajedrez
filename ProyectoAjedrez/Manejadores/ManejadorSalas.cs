﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesAjedrez;
using AccesoDatosAjedrez;

namespace Manejadores
{
    public class ManejadorSalas
    {
        SalasAccesoDatos asd = new SalasAccesoDatos();
        Sala es = new Sala();
        public void IngresarSala(Sala dato)
        {
            asd.InsertarSalas(dato);
        }
        public void ActualizarSala(Sala dato)
        {
            asd.ActualizarSala(dato);
        }
        public void EliminarSala(Sala dato)
        {
            asd.EliminarSala(dato.IdSala.ToString());
        }
        public List<Sala> MostrarSalas(string consulta)
        {
            var listSala = asd.ObtenerSalas(consulta);
            return listSala;
        }
        // Pendiente ComboBoxHotel
    }
}
