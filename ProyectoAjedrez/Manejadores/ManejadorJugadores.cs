﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesAjedrez;
using AccesoDatosAjedrez;

namespace Manejadores
{
   public class ManejadorJugadores
    {
        AccesoDatosJugadores aj = new AccesoDatosJugadores();
        Jugador ej = new Jugador();

        public void GuardarJugador(Jugador jugador)
        {
            aj.InsertarJugadores(jugador);
        }
        public void ActualizarJugador(Jugador jugador)
        {
            aj.Actualizar(jugador);
        }
        public void EliminarJugador(string jugador)
        {
            aj.EliminarJugador(jugador);
        }
        public List<VDatosJugadores> ObtenerJugador(string consulta)
        {
            var listjugador = aj.ObtenerJugadores(consulta);
            return listjugador;
        }
        public List<JugadorExtId> ExtraerIdJugadores()
        {
            var listPais = aj.ObtenderId();
            return listPais;
        }
    }
}
