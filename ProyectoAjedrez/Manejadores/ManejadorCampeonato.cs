﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesAjedrez;
using AccesoDatosAjedrez;

namespace Manejadores
{
  public  class ManejadorCampeonato
    {
        CampeonatosAccesoDatos ac = new CampeonatosAccesoDatos();
        ParticipantesAccesoDatos p = new ParticipantesAccesoDatos();
        Campeonato c = new Campeonato();
        public void InsertarCampeonato(Campeonato dato)
        {
            ac.InsertarCampeonatos(dato);
        }
        public void ActualizarCampeonato(Campeonato dato)
        {
            ac.ActualizarCampeonato(dato);
        }
        public void EliminarCampeonato(int dato)
        {
            ac.EliminarCampeonato(dato);
        }
        public List<Campeonato> MostrarCampeonato(string consulta)
        {
            var listCampeonato = ac.ObtenerCampeonato(consulta);
            return listCampeonato;
        }
        public List<ParticipantesExtId> ExtraerIdParticipante()
        {
            var listpart = p.ObtenerIdParticip();
            return listpart;
        }
    }
}
