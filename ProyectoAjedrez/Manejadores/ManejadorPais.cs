﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesAjedrez;
using AccesoDatosAjedrez;

namespace Manejadores
{ 
  
   public class ManejadorPais
    {

        Pais ep = new Pais();
        PaisesAccesoDatos ap = new PaisesAccesoDatos();
        public void InsertarPais(Pais dato)
        {
            ap.InsertarPaises(dato);
        }
        public void ActualizarPais(Pais dato)
        {
            ap.ActualizarPais(dato);
        }
        public void ELiminarPais(int dato)
        {
            ap.EliminarPais(dato);
        }
        public List<Pais> MostrarPais(string consulta)
        {
            var listPais = ap.ObtenerPaises(consulta);
            return listPais;
        }
        // Pendiente ComboBox PaisRepresentado
        public List<PaisesExId> ExtraerIdPais()
        {
            var listPais = ap.ExtraerId();
            return listPais;
        }
    }
}
