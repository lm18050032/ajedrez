﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesAjedrez;
using AccesoDatosAjedrez;

namespace Manejadores
{
    public class ManejadorReservaciones
    {
        Reserva er = new Reserva();
        ReservacionesAccesoDatos ar = new ReservacionesAccesoDatos();
        public void InsertarReservacion(Reserva dato)
        {
            ar.InsertarReservaciones(dato);
        }
        public void ActualizarResevacion(Reserva dato)
        {
            ar.ActualizarReservacion(dato);
        }
        public void ElimnarReservación(Reserva dato)
        {
            ar.EliminarReservacion(dato.IdReserva.ToString());
        }
        public List<Reserva> MostrarReservas(string consulta)
        {
            var listReserva = ar.ObtenerReservas(consulta);
            return listReserva;
        }
        // Pendiente de COmboBox para Hotel

        // Pendiente de ComboBox para Participante

    }
}
