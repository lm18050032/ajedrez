﻿using System;
using System.Windows.Forms;
using EntidadesAjedrez;
using Manejadores;

namespace PresentacionAjedres
{
    public partial class MenuParticipante : Form
    {
        ManejadorJugadores mj;
        ManejadorArbitro Ma;
        ManejadorParticipantes Mp;
       public static Arbitro ar;
      public static  Jugador j;
      public static  Participantes p;
        public static int Contar = 0;
        public MenuParticipante()
        {
            InitializeComponent();
            mj = new ManejadorJugadores();
            Ma = new ManejadorArbitro();
            Mp = new ManejadorParticipantes();
            ar = new Arbitro();
            j = new Jugador();
            p = new Participantes();
            cmbCategoria.Items.Add("Todos");
            cmbCategoria.Items.Add("Jugadores");
            cmbCategoria.Items.Add("Arbitros");
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            Contar = 0;
            FrmAddParticipantes ad = new FrmAddParticipantes();
            ad.ShowDialog();
        }
        void buscarJugador(string Consulta)
        {
            DtgDatos.DataSource = mj.ObtenerJugador(Consulta); 
        }
        void buscarArbitro(string Consulta)
        {
            DtgDatos.DataSource = Ma.ObtenerArbitro(Consulta);
        }
        void buscarParticipantes(string Consulta)
        {
            DtgDatos.DataSource = Mp.MostrarParticipantes(Consulta);
        }

        private void cmbCategoria_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbCategoria.Text.Equals("Todos"))
            {
                buscarParticipantes("");
            }
            if(cmbCategoria.Text.Equals("Jugadores"))
            {
                buscarJugador("");
            }
            if(cmbCategoria.Text.Equals("Arbitros"))
            {
                buscarArbitro("");
            }
        }
        void EliminarArbitros()
        {
            if (MessageBox.Show("Desea eliminar el Arbitro actual", "Eliminar Arbitro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                var idArbitro = DtgDatos.CurrentRow.Cells["IdArbitro"].Value.ToString();
                Ma.EliminarArbitro(idArbitro);
                buscarArbitro("");
            }
        }
        void EliminarJugador()
        {
            if (MessageBox.Show("Desea eliminar el Jugador actual", "Eliminar Jugador", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                var idArbitro = DtgDatos.CurrentRow.Cells["IdJugador"].Value.ToString();
                mj.EliminarJugador(idArbitro);
                buscarArbitro("");
            }
         }
        void EliminarParticipante()
        {
            var idParticipante = DtgDatos.CurrentRow.Cells["IdParticipante"].Value.ToString();
            Mp.EliminarParticipante(idParticipante);
            buscarParticipantes("");
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if(cmbCategoria.Text.Equals("Todos"))
            {
                EliminarParticipante();
                MessageBox.Show("Participante eliminado!");
            }
            if (cmbCategoria.Text.Equals("Jugadores"))
            {
                EliminarJugador();
                MessageBox.Show("Jugador eliminado!");
            }
            if (cmbCategoria.Text.Equals("Arbitros"))
            {
                EliminarArbitros();
                MessageBox.Show("Arbitro eliminado!");
            }
        }

        private void MenuParticipante_Load(object sender, EventArgs e)
        {
            cmbCategoria.SelectedIndex = 0;
            buscarParticipantes("");
        }

        private void DtgDatos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (cmbCategoria.Text.Equals("Jugadores"))
            {
                Jugador();
            }
            if (cmbCategoria.Text.Equals("Arbitros"))
            {
                EditarArb();
            }
         }

        void Jugador()
        {
                p.IdParticipante = int.Parse(DtgDatos.CurrentRow.Cells["IdJugador"].Value.ToString());
                p.Nombre = DtgDatos.CurrentRow.Cells["nombre"].Value.ToString();
                p.Direccion = DtgDatos.CurrentRow.Cells["Direccion"].Value.ToString();
                p.FkIdPais = int.Parse(DtgDatos.CurrentRow.Cells["FkIdPais"].Value.ToString());
                j.Nivel = int.Parse(DtgDatos.CurrentRow.Cells["Nivel"].Value.ToString());
        }
        void EditarArb()
        {

                p.IdParticipante = int.Parse(DtgDatos.CurrentRow.Cells["Idarbitro"].Value.ToString());
                p.Nombre = DtgDatos.CurrentRow.Cells["nombre"].Value.ToString();
                p.Direccion = DtgDatos.CurrentRow.Cells["Direccion"].Value.ToString();
                p.FkIdPais = int.Parse(DtgDatos.CurrentRow.Cells["FkIdPais"].Value.ToString());
       }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            if (cmbCategoria.Text.Equals("Jugadores"))
            {
                Contar = 1;
                FrmAddParticipantes ad = new FrmAddParticipantes();
                ad.CmbPais.SelectedValue = p.FkIdPais;
                ad.ShowDialog();
            }
            if (cmbCategoria.Text.Equals("Arbitros"))
            {
                Contar = 2;
                FrmAddParticipantes ad = new FrmAddParticipantes();
                ad.CmbPais.SelectedValue = p.FkIdPais;
                ad.ShowDialog();
            }
            if (cmbCategoria.Text.Equals("Todos"))
            {
                MessageBox.Show("Solo puedes modificar seleccionando la categoria JUGADOR O ARBITRO");
            }
     }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            if (cmbCategoria.Text.Equals("Todos"))
            {
                buscarParticipantes(TxtBuscar.Text);
            }
            if (cmbCategoria.Text.Equals("Jugadores"))
            {
                buscarJugador(TxtBuscar.Text);
            }
            if (cmbCategoria.Text.Equals("Arbitros"))
            {
                buscarArbitro(TxtBuscar.Text);
            }
        }
    }
}
