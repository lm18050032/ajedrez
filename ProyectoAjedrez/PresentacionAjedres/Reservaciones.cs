﻿using System;
using System.Windows.Forms;
using Manejadores;
using EntidadesAjedrez;

namespace PresentacionAjedres
{
    public partial class Reservaciones : Form
    {
        ManejadorReserva Mr;
        ManejadorHotel Mh;
        ManejadorParticipantes Mp;
        Reserva Er;
        int contar = 0;
        public Reservaciones()
        {
            InitializeComponent();
            Mr = new ManejadorReserva();
            Er = new Reserva();
            Mh = new ManejadorHotel();
            Mp = new ManejadorParticipantes();
            Er = new Reserva();
            CmbHoteles.ValueMember = "idHotel";
            CmbHoteles.DisplayMember = "nombre";
            CmbHoteles.DataSource = Mh.ExtraerIdHotel();

            CmbParticipantes.ValueMember = "Idparticipante";
            CmbParticipantes.DisplayMember = "Nombre";
            CmbParticipantes.DataSource = Mp.ExtraerIdParticipante();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Er.IdReserva = int.Parse(DtgDatos.CurrentRow.Cells["IdReserva"].Value.ToString());
            Er.FechaEntrada = DtgDatos.CurrentRow.Cells["FechaEntrada"].Value.ToString();
            Er.FechaSalida =DtgDatos.CurrentRow.Cells["FechaSalida"].Value.ToString();
            Er.FkIdHotel = int.Parse(DtgDatos.CurrentRow.Cells["FkIdHotel"].Value.ToString());
            Er.FkIdParticipantes= int.Parse(DtgDatos.CurrentRow.Cells["FkIdParticipantes"].Value.ToString());
        }

        private void Reservaciones_Load(object sender, EventArgs e)
        {
            CmbHoteles.ValueMember = "idHotel";
            CmbHoteles.DisplayMember = "nombre";
            CmbHoteles.DataSource = Mh.ExtraerIdHotel();

            CmbParticipantes.ValueMember = "Idparticipante";
            CmbParticipantes.DisplayMember = "nombre";
            CmbParticipantes.DataSource = Mp.ExtraerIdParticipante();
            Buscar("");
            Desabilitar();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            if(contar>0)
            {
                Modificar();
                Buscar("");
                LimpiarTexto();
                contar = 0;
            }
            else
            {
                try
                {
                    
                   GuardarReservacion();
                    Buscar("");
                    LimpiarTexto();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error" + ex);
                }
            }
           
        }
        
        void GuardarReservacion()
        {

            DateTime fechaE = DtmFechaEntrada.Value;
            var fechaS = DtmFechaSalida.Value;

                    Mr.GuardarReservacion(new Reserva
                    {
                        IdReserva = int.Parse(TxtCodigo.Text),
                        FechaEntrada = fechaE.ToString("yyyy/MM/dd"),
                        FechaSalida = fechaS.ToString("yyyy/MM/dd"),
                        FkIdHotel = int.Parse(CmbHoteles.SelectedValue.ToString()),
                        FkIdParticipantes = int.Parse(CmbParticipantes.SelectedValue.ToString())
                    });
        }
        void Buscar(string Consulta)
        {
            DtgDatos.DataSource = Mr.MostrarReservaciones(Consulta);
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            Buscar(TxtBuscar.Text);
        }
        void LimpiarTexto()
        {
            TxtCodigo.Text = "";
            CmbHoteles.SelectedIndex = 0;
            CmbParticipantes.SelectedIndex = 0;
            DtmFechaEntrada.Text = "";
            DtmFechaSalida.Text = "";
        }
        void EliminarReservacion()
        {
           var idreservacion = int.Parse(DtgDatos.CurrentRow.Cells["IdReserva"].Value.ToString());
            Mr.ELiminarReserva(idreservacion);
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar la reservación actual?", "Eliminar Reservación", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                EliminarReservacion();
                Buscar("");
                MessageBox.Show("Reservacion Eliminada");
            }
       }
        void Modificar()
        {
            DateTime fechaE = DtmFechaEntrada.Value;
            var fechaS = DtmFechaSalida.Value;
            Mr.ActualizarReservacion(new Reserva {
            IdReserva= int.Parse(TxtCodigo.Text),
            FechaEntrada= fechaE.ToString("yyyy/MM/dd"),
            FechaSalida= fechaS.ToString("yyyy/MM/dd"),
            FkIdHotel = int.Parse(CmbHoteles.SelectedValue.ToString()),
            FkIdParticipantes= int.Parse(CmbParticipantes.SelectedValue.ToString())
            });
            TxtCodigo.Enabled = true;
        }
        private void BtnModificar_Click(object sender, EventArgs e)
        {
            LimpiarTexto();
            try
            {
                contar = 1;
                TxtCodigo.Enabled = false;
                TxtCodigo.Text = Er.IdReserva.ToString();
                DtmFechaEntrada.Text = Er.FechaEntrada;
                DtmFechaSalida.Text = Er.FechaSalida;
                CmbHoteles.SelectedValue = Er.FkIdHotel;
                CmbParticipantes.SelectedValue = Er.FkIdParticipantes;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error"+ex);
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }
        void Desabilitar()
        {
            var Hotelesregis = Mh.ExtraerIdHotel();
            var ParticipantesRegis = Mp.ExtraerIdParticipante();
            if(Hotelesregis.Count<1 || ParticipantesRegis.Count<1)
            {
                TxtCodigo.Enabled = false;
                DtmFechaEntrada.Enabled = false;
                DtmFechaSalida.Enabled = false;
                BtnGuardar.Enabled = false;
                MessageBox.Show("No podra realizar reservaciones, favor de revisar si hay hoteles y participantes registrados");
            }
        }
        void Verificar()
        {
            for (int i = 0; i < DtgDatos.RowCount; i++)
            {
               
                if(DtgDatos.Rows[i].Cells[5].Value.ToString().Equals(CmbParticipantes.SelectedValue))
                {
                    MessageBox.Show("Error, el participante ya fue registrado");
                }
            }
        }
      
    }
}
