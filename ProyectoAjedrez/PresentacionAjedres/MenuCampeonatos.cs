﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Manejadores;
using EntidadesAjedrez;

namespace PresentacionAjedres
{
    public partial class MenuCampeonatos : Form
    {
        ManejadorCampeonato MC;
        Campeonato C;
        public static int contar = 0;
        public MenuCampeonatos()
        {
            C = new Campeonato();
            MC = new ManejadorCampeonato();
            InitializeComponent();
        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {
            contar = 1;
            Campeonatos fc = new Campeonatos(C);
            fc.ShowDialog();
            contar = 0;
        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            Campeonatos fc = new Campeonatos();
            fc.ShowDialog();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea Eliminar El Campeonato Actual?", "Eliminar Campeonato", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                Buscar("");
                MessageBox.Show("F por el Campeonato Eliminado");
            }
        }

        public void Eliminar()
        {
            var idcampeonato = int.Parse(DgvMenuCampeonato.CurrentRow.Cells["IdCampeonato"].Value.ToString());
            MC.EliminarCampeonato(idcampeonato);
        }
        void Buscar(string Consulta)
        {

            DgvMenuCampeonato.DataSource = MC.MostrarCampeonato(Consulta);
        }

        private void MenuCampeonatos_Load(object sender, EventArgs e)
        {
            Buscar("");
        }

        private void TxtAgregar_TextChanged(object sender, EventArgs e)
        {
            Buscar(TxtBuscar.Text);
        }

        private void DgvMenuCampeonato_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            C.IdCampeonato = int.Parse(DgvMenuCampeonato.CurrentRow.Cells["IdCampeonato"].Value.ToString());
            C.Nombre = DgvMenuCampeonato.CurrentRow.Cells["nombre"].Value.ToString();
            C.Tipo = DgvMenuCampeonato.CurrentRow.Cells["Tipo"].Value.ToString();
            C.FkIdParticipante = int.Parse(DgvMenuCampeonato.CurrentRow.Cells["FkIdparticipante"].Value.ToString());
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
