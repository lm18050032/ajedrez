﻿using System;
using System.Windows.Forms;
using Manejadores;
using EntidadesAjedrez;

namespace PresentacionAjedres
{
    public partial class Campeonatos : Form
    {
        ManejadorCampeonato MC;
        ParticipantesExtId px;
        public Campeonatos()
        {
            InitializeComponent();
            MC = new ManejadorCampeonato();
            px = new ParticipantesExtId();
            CmbParticipante.ValueMember = "Idparticipante";
            CmbParticipante.DisplayMember = "nombre";
            CmbParticipante.DataSource = MC.ExtraerIdParticipante();
        }
        public Campeonatos(Campeonato c)
        {
            InitializeComponent();
            MC = new ManejadorCampeonato();
            TxtCodCampeonato.Text = c.IdCampeonato.ToString();
            TxtNombre.Text = c.Nombre;
            TxtTipo.Text = c.Tipo.ToString();
            CmbParticipante.SelectedValue = c.FkIdParticipante.ToString();
            TxtCodCampeonato.Enabled = false;
            CmbParticipante.ValueMember = "Idparticipante";
            CmbParticipante.DisplayMember = "nombre";
            CmbParticipante.DataSource = MC.ExtraerIdParticipante();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            Guardar();
        }
        private void Guardar()
        {

            if (MenuCampeonatos.contar > 0)
            {
                Actualizar();
                Close();
            }
            else
                try
                {
                    Insertar();
                    Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error" + ex);

                }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Campeonatos_Load(object sender, EventArgs e)
        {
            CmbParticipante.ValueMember = "Idparticipante";
            CmbParticipante.DisplayMember = "nombre";
            CmbParticipante.DataSource = MC.ExtraerIdParticipante();
            Desabilitar();
        }
        void Actualizar()
        {
            MC.ActualizarCampeonato(new Campeonato
            {
                IdCampeonato = int.Parse(TxtCodCampeonato.Text),
                Nombre = TxtNombre.Text,
                Tipo = TxtTipo.Text,
                FkIdParticipante = int.Parse(CmbParticipante.SelectedValue.ToString())
            });
        }
        void Insertar()
        {

                MC.InsertarCampeonato(new Campeonato
                {
                    IdCampeonato = int.Parse(TxtCodCampeonato.Text),
                    Nombre = TxtNombre.Text,
                    Tipo = TxtTipo.Text,
                    FkIdParticipante = int.Parse(CmbParticipante.SelectedValue.ToString())
                });
        }
        void Desabilitar()
        {
            var Participantes  = MC.ExtraerIdParticipante();
            if(Participantes.Count<1)
            {
                TxtCodCampeonato.Enabled = false;
                TxtNombre.Enabled = false;
                TxtTipo.Enabled = false;
                BtnGuardar.Enabled = false;
                MessageBox.Show("Error, favor de verificar que exista registro de participantes");
            }
        }
    }
}
