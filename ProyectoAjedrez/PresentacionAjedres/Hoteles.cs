﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
 using EntidadesAjedrez;
using Manejadores;
using System.Windows.Forms;

namespace PresentacionAjedres
{
    public partial class Hoteles : Form
    {
        ManejadorHotel mh;
        Hotel h;
        public static int contar = 0;
        public Hoteles()
        {
            h = new Hotel();
            mh = new ManejadorHotel();
            InitializeComponent();
           
        }
        private void button4_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Hotel_Load(object sender, EventArgs e)
        {
            Buscar("");
        }

        private void Guardar()
        {

            if (contar > 0)
            {
                Actualizar();
                LimpiarBotones();
                Buscar("");
                contar = 0;
            }
            else
            if (contar == 0)
            {
                try
                {
                    Insertar();
                    LimpiarBotones();
                    Buscar("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error" + ex);

                }
            }
        }
        void Actualizar()
        {
            mh.ActualizarMotel(new Hotel
            {
                IdHotel = int.Parse(txtcodigo.Text),
                Nombre = txtnombre.Text,
                Direccion = txtdireccion.Text,
                Telefono = txttelefono.Text
              }) ;
            txtcodigo.Enabled = true;
        }

        void Insertar()
        {
          
                mh.InsertarMotel(new Hotel
                {
                    IdHotel = int.Parse(txtcodigo.Text),
                    Nombre = txtnombre.Text,
                    Direccion = txtdireccion.Text,
                    Telefono = txttelefono.Text
                });
        }

        void Buscar(string Consulta)
        {

            DtvHotel.DataSource = mh.MostrarMotel(Consulta);
        }

        private void MenuPaises_Load(object sender, EventArgs e)
        {
            Buscar("");
        }
        public void Eliminar()
        {
            var idhotel = int.Parse(DtvHotel.CurrentRow.Cells["idHotel"].Value.ToString());
            mh.EliminarMotel(idhotel);
        }
        private void btneliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar este hotel actual?", "Eliminar Hotel", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                Buscar("");
                MessageBox.Show("Hotel eliminado");
            }
        }

        private void txtbuscar_TextChanged(object sender, EventArgs e)
        {
            Buscar(txtbuscar.Text);
        }
        private void DtvHotel_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            h.IdHotel = int.Parse(DtvHotel.CurrentRow.Cells["idHotel"].Value.ToString());
            h.Nombre = DtvHotel.CurrentRow.Cells["nombre"].Value.ToString();
            h.Direccion = DtvHotel.CurrentRow.Cells["Direccion"].Value.ToString();
            h.Telefono = DtvHotel.CurrentRow.Cells["Telefono"].Value.ToString();
        }

        private void btnguardar_Click(object sender, EventArgs e)
        {

            Guardar();
            contar = 0;
        }

        private void btnmodificar_Click(object sender, EventArgs e)
        {
            contar = 1;
            txtcodigo.Enabled = false;
            txtcodigo.Text = h.IdHotel.ToString();
            txtnombre.Text = h.Nombre;
            txtdireccion.Text = h.Direccion;
            txttelefono.Text = h.Telefono;
        }

        private void btnsala_Click(object sender, EventArgs e)
        {
            Salas s = new Salas();
            s.ShowDialog();

        }
        void LimpiarBotones()
        {
            txtcodigo.Text = "";
            txtnombre.Text = "";
            txtdireccion.Text = "";
            txttelefono.Text = "";
        }
    }

}

