﻿using System;
using System.Windows.Forms;
using Manejadores;
using EntidadesAjedrez;
namespace PresentacionAjedres
{
    public partial class Salas : Form
    {
        ManejadorSala ms;
        ManejadorHotel h;
        Sala s;
        int contar = 0;
        public Salas()
        {
            InitializeComponent();
            ms = new ManejadorSala();
            h = new ManejadorHotel();
            s = new Sala();
            cmbhotel.ValueMember = "idHotel";
            cmbhotel.DisplayMember = "nombre";
            cmbhotel.DataSource = h.ExtraerIdHotel();
          
        }
        public void Eliminar()
        {
            var idsala = int.Parse(DvgSala.CurrentRow.Cells["idSala"].Value.ToString());
            ms.ELiminarSala(idsala);
        }
        void Buscar(string Consulta)
        {

           DvgSala.DataSource = ms.MostrarSala(Consulta);
        }

        private void txtbuscar_TextChanged(object sender, EventArgs e)
        {
            Buscar(txtbuscar.Text);
        }

        private void Sala_Load(object sender, EventArgs e)
        {
            Buscar("");
            cmbhotel.ValueMember = "idHotel";
            cmbhotel.DisplayMember = "nombre";
            cmbhotel.DataSource = h.ExtraerIdHotel();
        }

        private void Guardar()
        {

            if (contar > 0)
            {
                Actualizar();
                Buscar("");
                LimpiarTexto();
                contar = 0;

            }
            else
            if (contar == 0)
            {
                try
                {
                    Insertar();
                    Buscar("");
                    LimpiarTexto();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error" + ex);

                }
            }
        }

        void Actualizar()
        {
            ms.ActualizarSala(new Sala
            {
                IdHotel = int.Parse(cmbhotel.SelectedValue.ToString()),
                IdSala = int.Parse(txtsala.Text),
                Capacidad = int.Parse(txtcapa.Text),
                Medios = txtmedios.Text
            });
            txtsala.Enabled = true;
        }
        void Insertar()
        {
                ms.InsertarSala(new Sala
                {
                    IdHotel = int.Parse(cmbhotel.SelectedValue.ToString()),
                    IdSala = int.Parse(txtsala.Text),
                    Capacidad = int.Parse(txtcapa.Text),
                    Medios = txtmedios.Text
                });
        }

        private void DvgSala_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            s.IdHotel = int.Parse(DvgSala.CurrentRow.Cells["idHotel"].Value.ToString());
            s.IdSala = int.Parse(DvgSala.CurrentRow.Cells["idSala"].Value.ToString());
            s.Capacidad = int.Parse(DvgSala.CurrentRow.Cells["Capacidad"].Value.ToString());
            s.Medios = DvgSala.CurrentRow.Cells["Medios"].Value.ToString();
        }

        private void btneliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar la sala actual?", "Eliminar Sala", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                Buscar("");
                MessageBox.Show("sala eliminado");
            }
        }

        private void btnregresar_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void btnaceptar_Click(object sender, EventArgs e)
        {
            Guardar();
        }

        private void btnmodificar_Click(object sender, EventArgs e)
        {
            contar = 1;
            txtsala.Enabled = false;
            cmbhotel.SelectedValue = s.IdHotel;
            txtsala.Text = s.IdSala.ToString();
            txtcapa.Text = s.Capacidad.ToString(); ;
            txtmedios.Text = s.Medios;
        }
        void LimpiarTexto()
        {
            txtsala.Text = "";
            txtcapa.Text = "";
            txtmedios.Text = "";
        }
    }
}
