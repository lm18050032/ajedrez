﻿using System;
using System.Windows.Forms;
using EntidadesAjedrez;
using Manejadores;

namespace PresentacionAjedres
{
    public partial class FrmAddParticipantes : Form
    {
        ManejadorJugadores Mj;
        ManejadorArbitro Ma;
        ManejadorParticipantes Mp;
        Jugador j;
        ManejadorPais MPais;
        Arbitro ar;
        Participantes par;
        public FrmAddParticipantes()
        {
            InitializeComponent();
            Mj = new ManejadorJugadores();
            Ma = new ManejadorArbitro();
            Mp = new ManejadorParticipantes();
            j = new Jugador();
            ar = new Arbitro();
            TxtNivel.Enabled = false;
            MPais = new ManejadorPais();
            par = new Participantes();
            CmbPais.ValueMember = "idpais";
            CmbPais.DisplayMember = "nombre";
            CmbPais.DataSource = MPais.ExtraerIdPais();
            CmbTipo.Items.Add("Arbitro");
            CmbTipo.Items.Add("Jugador");
            if(MenuParticipante.Contar==1)
            {
                CmbTipo.SelectedIndex = 1;
                CmbTipo.Enabled = false;
                TxtCodigo.Enabled = false;
                TxtCodigo.Text = MenuParticipante.p.IdParticipante.ToString();
                TxtNombre.Text = MenuParticipante.p.Nombre.ToString();
                TxtDireccion.Text = MenuParticipante.p.Direccion.ToString();
                //CmbPais.SelectedValue = MenuParticipante.p.FkIdPais;
                TxtNivel.Text = MenuParticipante.j.Nivel.ToString();
            }
            if(MenuParticipante.Contar == 2)
            {
                CmbTipo.SelectedIndex = 0;
                TxtCodigo.Enabled = false;
                CmbTipo.Enabled = false;
                TxtCodigo.Text= MenuParticipante.p.IdParticipante.ToString();
                TxtNombre.Text = MenuParticipante.p.Nombre.ToString();
                TxtDireccion.Text = MenuParticipante.p.Direccion.ToString();
               // CmbPais.SelectedValue = MenuParticipante.p.FkIdPais;
            }
        }
            private void BtnGuardar_Click(object sender, EventArgs e)
        {
            GuardarDatos();
        }
        void GuardarDatos()
        {
            if (MenuParticipante.Contar > 0)
            {
                if (MenuParticipante.Contar == 1)
                {
                    ActualizarParticipante();
                    ActualizarJugador();
                    MessageBox.Show("Correcto");
                    Close();
                }
                if (MenuParticipante.Contar == 2)
                {
                    ActualizarParticipante();
                    MessageBox.Show("Correcto");
                    Close();
                }
            }
            else
            {
                try
                {
                  
                    if (!CmbTipo.Text.Equals(""))
                    {
                        par.IdParticipante = int.Parse(TxtCodigo.Text);
                        par.Nombre = TxtNombre.Text;
                        par.Direccion = TxtDireccion.Text;
                        par.FkIdPais = int.Parse(CmbPais.SelectedValue.ToString());
                        Mp.InsertarParticipante(par);
                        if (CmbTipo.Text.Equals("Jugador"))
                        {
                            j.IdJugador = int.Parse(TxtCodigo.Text);
                            j.Nivel = int.Parse(TxtNivel.Text);
                            Mj.GuardarJugador(j);
                        }
                        if (CmbTipo.Text.Equals("Arbitro"))
                        {
                            ar.IdArbitro = int.Parse(TxtCodigo.Text);
                            Ma.GuardarArbitro(ar);
                        }
                        Close();
                    }
                    else
                        MessageBox.Show("Error, Selecciones el tipo de participante");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error" + ex);
                }

            }
       }
        private void CmbTipo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CmbTipo.Text.Equals("Jugador"))
                TxtNivel.Enabled = true;
        }
        void ActualizarParticipante()
        {
            Mp.ActualizarParticipante(new Participantes {
                IdParticipante = int.Parse(TxtCodigo.Text),
                Nombre = TxtNombre.Text,
                Direccion = TxtDireccion.Text,
                FkIdPais = int.Parse(CmbPais.SelectedValue.ToString())
            });
        }
        void ActualizarJugador()
        {
            Mj.ActualizarJugador(new Jugador {
                IdJugador = int.Parse(TxtCodigo.Text),
                Nivel = int.Parse(TxtNivel.Text)

            });
          }
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FrmAddParticipantes_Load(object sender, EventArgs e)
        {
            var cantidad = MPais.ExtraerIdPais();
            if (cantidad.Count < 1)
            {
                MessageBox.Show("No podras registrar participantes, por favor revisa que existan paises registrados");
                Desabilitar();
            }
        }
        void Desabilitar()
        {
            TxtCodigo.Enabled = false;
            TxtNombre.Enabled = false;
            TxtDireccion.Enabled = false;
            CmbTipo.Enabled = false;
        }
    }
}
