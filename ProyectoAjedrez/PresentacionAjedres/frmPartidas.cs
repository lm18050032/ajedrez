﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Manejadores;
using EntidadesAjedrez;

namespace PresentacionAjedres
{
    public partial class frmPartidas : Form
    {
        Partidas ep;
        ManejadorPartidas mp;
        public frmPartidas()
        {
            InitializeComponent();
            mp = new ManejadorPartidas();
            
        }

        private void frmPartidas_Load(object sender, EventArgs e)
        {
            btnModificar.Enabled = false;
            Refrescar();
            cmbColor1.Items.Add("Blanco");
            cmbColor1.Items.Add("Negro");
            cmbColor1.SelectedIndex = 0;
            cmbJugador1.ValueMember = "Idparticipante";
            cmbJugador1.DisplayMember = "Nombre";
            cmbJugador1.DataSource = mp.EstraeridJugador();
            cmbJugador2.ValueMember = "Idparticipante";
            cmbJugador2.DisplayMember = "Nombre";
            cmbJugador2.DataSource = mp.EstraeridJugador();
            cmbArbitro.ValueMember = "Idparticipante";
            cmbArbitro.DisplayMember = "Nombre";
            cmbArbitro.DataSource = mp.ExtraerIdArb();
            cmbHotel.ValueMember = "Idhotel";
            cmbHotel.DisplayMember = "nombre";
            cmbHotel.DataSource = mp.ExtraerIdHOT();
            cmbSala.ValueMember = "IdSala";
            cmbSala.DisplayMember = "IdSala";
            cmbSala.DataSource = mp.ExtraerIdSAL();
            Colorsito();
        }

        public void Refrescar()
        {
            dtgPartida.DataSource = mp.MostrarPartidas(txtBuscar.Text);
            ep = new Partidas();

        }

        private void cmbColor1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Colorsito();
        }
        void Colorsito()
        {
            if(cmbColor1.SelectedIndex==0)
            {
                lblColor.Text = "Negro";
            }
            else
                lblColor.Text = "Blanco";
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            var date = dtpJornada.Value.ToString("yyyy/MM/dd");
            ep.IdPartidas = int.Parse(txtCodigo.Text);
            ep.Jornada = date;
            ep.Color1 = cmbColor1.SelectedItem.ToString();
            ep.Color2 = lblColor.Text;
            ep.FkIdArbitro = int.Parse(cmbArbitro.SelectedValue.ToString());
            ep.FkIdHotel = int.Parse(cmbHotel.SelectedValue.ToString());
            ep.FkIdJugador1 = int.Parse(cmbJugador1.SelectedValue.ToString());
            ep.FkIdJugador2 = int.Parse(cmbJugador2.SelectedValue.ToString());
            ep.FkIdSala = int.Parse(cmbSala.SelectedValue.ToString());
            ep.Entradas = int.Parse(txtEntradas.Text);
            mp.IngresarPartida(ep);
            Refrescar();
            MessageBox.Show("Correcto");
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            
            if (MessageBox.Show("Desea eliminar la partida con el código: " + ep.IdPartidas.ToString() + "?", "Eliminar Partida", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                mp.EliminarPartida(ep.IdPartidas.ToString());
                MessageBox.Show("Partida eliminada");
            }
            


            Refrescar();
        }

        private void dtgPartida_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
            ep.IdPartidas = int.Parse(dtgPartida.CurrentRow.Cells["Codpartida"].Value.ToString());
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnComentarios_Click(object sender, EventArgs e)
        {
            frmComentarios fc = new frmComentarios();
            fc.ShowDialog();
        }

        private void dtgPartida_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            ep.IdPartidas = int.Parse(dtgPartida.CurrentRow.Cells["Codpartida"].Value.ToString());
        }
    }
}
