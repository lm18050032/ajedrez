﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PresentacionAjedres
{
    public partial class FrmMenuPrincipal : Form
    {
        public FrmMenuPrincipal()
        {
            InitializeComponent();
        }

        private void registrosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MenuPaises p = new MenuPaises();
            p.ShowDialog();
        }

        private void paisesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MenuParticipante mp = new MenuParticipante();
            mp.ShowDialog();
        }

        private void hotelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Hoteles p = new Hoteles();
            p.ShowDialog();
        }

        private void campeonatosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MenuCampeonatos mc = new MenuCampeonatos();
            mc.ShowDialog();
        }

        private void reservacionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Reservaciones r = new Reservaciones();
            r.ShowDialog();
        }

        private void partidosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPartidas fp = new frmPartidas();
            fp.ShowDialog();
        }
    }
}
