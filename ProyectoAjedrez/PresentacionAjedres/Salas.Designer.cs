﻿namespace PresentacionAjedres
{
    partial class Salas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtmedios = new System.Windows.Forms.TextBox();
            this.txtcapa = new System.Windows.Forms.TextBox();
            this.txtsala = new System.Windows.Forms.TextBox();
            this.cmbhotel = new System.Windows.Forms.ComboBox();
            this.DvgSala = new System.Windows.Forms.DataGridView();
            this.btnaceptar = new System.Windows.Forms.Button();
            this.btnmodificar = new System.Windows.Forms.Button();
            this.btneliminar = new System.Windows.Forms.Button();
            this.btnregresar = new System.Windows.Forms.Button();
            this.txtbuscar = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DvgSala)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 82);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "CodigoHotel";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 165);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "CodigoSala";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 247);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Capacidad";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 338);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Medios";
            // 
            // txtmedios
            // 
            this.txtmedios.Location = new System.Drawing.Point(26, 378);
            this.txtmedios.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtmedios.Name = "txtmedios";
            this.txtmedios.Size = new System.Drawing.Size(290, 28);
            this.txtmedios.TabIndex = 5;
            // 
            // txtcapa
            // 
            this.txtcapa.Location = new System.Drawing.Point(26, 292);
            this.txtcapa.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtcapa.Name = "txtcapa";
            this.txtcapa.Size = new System.Drawing.Size(290, 28);
            this.txtcapa.TabIndex = 6;
            // 
            // txtsala
            // 
            this.txtsala.Location = new System.Drawing.Point(26, 200);
            this.txtsala.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtsala.Name = "txtsala";
            this.txtsala.Size = new System.Drawing.Size(290, 28);
            this.txtsala.TabIndex = 7;
            // 
            // cmbhotel
            // 
            this.cmbhotel.FormattingEnabled = true;
            this.cmbhotel.Location = new System.Drawing.Point(26, 121);
            this.cmbhotel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbhotel.Name = "cmbhotel";
            this.cmbhotel.Size = new System.Drawing.Size(290, 28);
            this.cmbhotel.TabIndex = 8;
            // 
            // DvgSala
            // 
            this.DvgSala.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DvgSala.Location = new System.Drawing.Point(338, 120);
            this.DvgSala.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.DvgSala.Name = "DvgSala";
            this.DvgSala.Size = new System.Drawing.Size(448, 286);
            this.DvgSala.TabIndex = 9;
            this.DvgSala.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DvgSala_CellClick);
            // 
            // btnaceptar
            // 
            this.btnaceptar.Location = new System.Drawing.Point(126, 431);
            this.btnaceptar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnaceptar.Name = "btnaceptar";
            this.btnaceptar.Size = new System.Drawing.Size(101, 52);
            this.btnaceptar.TabIndex = 10;
            this.btnaceptar.Text = "Guardar";
            this.btnaceptar.UseVisualStyleBackColor = true;
            this.btnaceptar.Click += new System.EventHandler(this.btnaceptar_Click);
            // 
            // btnmodificar
            // 
            this.btnmodificar.Location = new System.Drawing.Point(383, 435);
            this.btnmodificar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnmodificar.Name = "btnmodificar";
            this.btnmodificar.Size = new System.Drawing.Size(109, 52);
            this.btnmodificar.TabIndex = 11;
            this.btnmodificar.Text = "Modificar";
            this.btnmodificar.UseVisualStyleBackColor = true;
            this.btnmodificar.Click += new System.EventHandler(this.btnmodificar_Click);
            // 
            // btneliminar
            // 
            this.btneliminar.Location = new System.Drawing.Point(256, 438);
            this.btneliminar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btneliminar.Name = "btneliminar";
            this.btneliminar.Size = new System.Drawing.Size(101, 47);
            this.btneliminar.TabIndex = 12;
            this.btneliminar.Text = "Eliminar";
            this.btneliminar.UseVisualStyleBackColor = true;
            this.btneliminar.Click += new System.EventHandler(this.btneliminar_Click);
            // 
            // btnregresar
            // 
            this.btnregresar.Location = new System.Drawing.Point(531, 438);
            this.btnregresar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnregresar.Name = "btnregresar";
            this.btnregresar.Size = new System.Drawing.Size(109, 47);
            this.btnregresar.TabIndex = 13;
            this.btnregresar.Text = "Cancelar";
            this.btnregresar.UseVisualStyleBackColor = true;
            this.btnregresar.Click += new System.EventHandler(this.btnregresar_Click);
            // 
            // txtbuscar
            // 
            this.txtbuscar.Location = new System.Drawing.Point(427, 82);
            this.txtbuscar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtbuscar.Name = "txtbuscar";
            this.txtbuscar.Size = new System.Drawing.Size(359, 28);
            this.txtbuscar.TabIndex = 14;
            this.txtbuscar.TextChanged += new System.EventHandler(this.txtbuscar_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(347, 85);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 20);
            this.label7.TabIndex = 18;
            this.label7.Text = "Buscar";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(290, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(113, 23);
            this.label5.TabIndex = 19;
            this.label5.Text = "Registrar sala";
            // 
            // Salas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(814, 496);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtbuscar);
            this.Controls.Add(this.btnregresar);
            this.Controls.Add(this.btneliminar);
            this.Controls.Add(this.btnmodificar);
            this.Controls.Add(this.btnaceptar);
            this.Controls.Add(this.DvgSala);
            this.Controls.Add(this.cmbhotel);
            this.Controls.Add(this.txtsala);
            this.Controls.Add(this.txtcapa);
            this.Controls.Add(this.txtmedios);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Salas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sala";
            this.Load += new System.EventHandler(this.Sala_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DvgSala)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtmedios;
        private System.Windows.Forms.TextBox txtcapa;
        private System.Windows.Forms.TextBox txtsala;
        private System.Windows.Forms.ComboBox cmbhotel;
        private System.Windows.Forms.DataGridView DvgSala;
        private System.Windows.Forms.Button btnaceptar;
        private System.Windows.Forms.Button btnmodificar;
        private System.Windows.Forms.Button btneliminar;
        private System.Windows.Forms.Button btnregresar;
        private System.Windows.Forms.TextBox txtbuscar;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
    }
}