﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Manejadores;
using EntidadesAjedrez;

namespace PresentacionAjedres
{
    public partial class frmComentarios : Form
    {
        int opc = 0;
        ManejadorMovimiento mm;
        Movimiento em;
        public frmComentarios()
        {
            InitializeComponent();
            mm = new ManejadorMovimiento();
        }

        private void frmComentarios_Load(object sender, EventArgs e)
        {
            cmbPartida.ValueMember = "IdPartidas";
            cmbPartida.DisplayMember = "IdPartidas";
            cmbPartida.DataSource = mm.ExtraerIdPartida();
            Refrescar();
        }
        void Refrescar()
        {
            dtgComentarios.DataSource = mm.MostrarMovimiento(txtBuscar.Text);
            em = new Movimiento(); 
            try
            {
                cmbPartida.SelectedIndex = 0;
            }
            catch (Exception)
            {

              
            }
            txtCodMov.Clear();
            txtComentario.Clear();
            txtMovimiento.Clear();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (opc==1)
            {
                Modificar();
            }
            else
            Guardar();
            Refrescar();
        }
        void Guardar()
        {
            em.IdPartidas = int.Parse(cmbPartida.SelectedValue.ToString());
            em.IdMovimiento = int.Parse(txtCodMov.Text);
            em.Movimientos = txtMovimiento.Text;
            em.Comentario = txtComentario.Text;
            mm.InsertarMovimiento(em);
        }
        void Modificar()
        {
            em.IdMovimiento = int.Parse(txtCodMov.Text);
            em.Movimientos = txtMovimiento.Text;
            em.Comentario = txtComentario.Text;
            mm.ActualizarMovimiento(em);
            cmbPartida.Enabled = true;
            btnModificar.Enabled = true;
            opc = 0;
        }
        private void dtgComentarios_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            em.IdPartidas = int.Parse(dtgComentarios.CurrentRow.Cells["IdPartidas"].Value.ToString());
            em.IdMovimiento = int.Parse(dtgComentarios.CurrentRow.Cells["IdMovimiento"].Value.ToString());
            em.Movimientos = dtgComentarios.CurrentRow.Cells["Movimientos"].Value.ToString();
            em.Comentario = dtgComentarios.CurrentRow.Cells["Comentario"].Value.ToString();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {

                opc = 1;
                cmbPartida.SelectedValue = em.IdPartidas;
                txtCodMov.Text = em.IdMovimiento.ToString();

                txtMovimiento.Text = em.Movimientos.ToString();
                txtComentario.Text = em.Comentario.ToString();
                btnModificar.Enabled = false;
            }
            catch (Exception)
            {

                btnModificar.Enabled = true;
            }

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar el movimiento con el código: " + em.IdMovimiento.ToString() + "?", "Eliminar Partida", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                mm.EliminarMovimiento(em);
                MessageBox.Show("Movimiento Eliminado");
            }
            Refrescar();
            
        }

        private void dtgComentarios_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            em.IdPartidas = int.Parse(dtgComentarios.CurrentRow.Cells["IdPartidas"].Value.ToString());
            em.IdMovimiento = int.Parse(dtgComentarios.CurrentRow.Cells["IdMovimiento"].Value.ToString());
            em.Movimientos = dtgComentarios.CurrentRow.Cells["Movimientos"].Value.ToString();
            em.Comentario = dtgComentarios.CurrentRow.Cells["Comentario"].Value.ToString();
        }
    }
}
