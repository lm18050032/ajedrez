﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Manejadores;
using EntidadesAjedrez;

namespace PresentacionAjedres
{
    public partial class Paises : Form
    {
        ManejadorPais MP;
        PaisesExId PE;
        public Paises()
        {
            InitializeComponent();
           PE = new PaisesExId();
            MP = new ManejadorPais();
                CmbPaisRep.ValueMember = "idpais";
                CmbPaisRep.DisplayMember = "nombre";
                CmbPaisRep.DataSource = MP.ExtraerIdPais();
        }
        public Paises(Pais p)
        {
            InitializeComponent();
           MP = new ManejadorPais();
            TxtCodPais.Text = p.IdPais.ToString();
            TxtNombre.Text = p.Nombre;
            TxtNoClubs.Text = p.NumeroClubs.ToString();
          //  CmbPaisRep.SelectedValue = p.Representa.ToString();
            TxtCodPais.Enabled = false;
            CmbPaisRep.ValueMember = "idpais";
            CmbPaisRep.DisplayMember = "nombre";
            CmbPaisRep.DataSource = MP.ExtraerIdPais();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            Guardar();
        }
        private void Guardar()
        {
            
                if (MenuPaises.contar>0)
                {
                Actualizar();
                    Close();
                }
                else
                if(MenuPaises.contar ==0)
                {
                try
                {
                    Insertar();
                    Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error" + ex);
                    
                }
                } 
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Paises_Load(object sender, EventArgs e)
        {
            Desabilitar();
                CmbPaisRep.ValueMember = "idpais";
                CmbPaisRep.DisplayMember = "nombre";
                CmbPaisRep.DataSource = MP.ExtraerIdPais();

        }
      void Actualizar()
        {
            MP.ActualizarPais(new Pais
            {
                IdPais = int.Parse(TxtCodPais.Text),
                Nombre = TxtNombre.Text,
                NumeroClubs = int.Parse(TxtNoClubs.Text),
                Representa = int.Parse(CmbPaisRep.SelectedValue.ToString())
            });
        }
        void Insertar()
        {
            var paises = MP.ExtraerIdPais();
            if (paises.Count < 1)
            {
                MP.InsertarPais(new Pais
                {
                    IdPais = int.Parse(TxtCodPais.Text),
                    Nombre = TxtNombre.Text,
                    NumeroClubs = int.Parse(TxtNoClubs.Text),
                    Representa = int.Parse(TxtCodPais.Text)
                });
            }
            else
            {
                MP.InsertarPais(new Pais
                {
                    IdPais = int.Parse(TxtCodPais.Text),
                    Nombre = TxtNombre.Text,
                    NumeroClubs = int.Parse(TxtNoClubs.Text),
                    Representa = int.Parse(CmbPaisRep.SelectedValue.ToString())
                });
            }
        }

        private void TxtNombre_TextChanged(object sender, EventArgs e)
        {
            PaisesVacios();
        }
        void PaisesVacios()
        {
            var paises = MP.ExtraerIdPais();
            List<string> pais;
            if (paises.Count <1)
            {

                pais = new List<string>();
                pais.Add(TxtNombre.Text);
                CmbPaisRep.DataSource = pais;
                CmbPaisRep.SelectedIndex = 0;
            }
        }
        void Desabilitar()
        {
            if (TxtCodPais.Text.Trim() == "")
            {
                TxtNombre.Enabled = false;
                TxtNoClubs.Enabled = false;
            }
            else
            {
                TxtNombre.Enabled = true;
                TxtNoClubs.Enabled = true;
            }
        }

        private void TxtCodPais_TextChanged(object sender, EventArgs e)
        {
            Desabilitar();
        }
    }
}
