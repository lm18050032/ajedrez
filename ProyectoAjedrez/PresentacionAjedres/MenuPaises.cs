﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Manejadores;
using EntidadesAjedrez;

namespace PresentacionAjedres
{
    public partial class MenuPaises : Form
    {
        ManejadorPais MP;
        Pais P;
        public static int contar=0;
        public MenuPaises()
        {
           P = new Pais();
            MP = new ManejadorPais();
            InitializeComponent();
        }
        private void BtnModificar_Click(object sender, EventArgs e)
        {
            
            Paises Pai = new Paises(P);
            contar = 1;
            Pai.CmbPaisRep.SelectedValue = P.Representa;
            Pai.ShowDialog();
            Buscar("");
            
        }
        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            contar = 0;
            Paises p = new Paises();
            p.ShowDialog();
            Buscar("");
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
          if (MessageBox.Show("Desea eliminar el pais actual?", "Eliminar Pais", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                Buscar("");
                MessageBox.Show("País eliminado");
            }
        }
        public void Eliminar()
        {
         var idpais = int.Parse(DgvMenuPais.CurrentRow.Cells["IdPais"].Value.ToString());
            MP.ELiminarPais(idpais);
        }
        void Buscar(string Consulta)
        {
          
              DgvMenuPais.DataSource = MP.MostrarPais(Consulta);
        }

        private void MenuPaises_Load(object sender, EventArgs e)
        {
            Buscar("");
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            Buscar(TxtBuscar.Text);
        }

        private void DgvMenuPais_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            P.IdPais = int.Parse(DgvMenuPais.CurrentRow.Cells["idpais"].Value.ToString());
            P.Nombre = DgvMenuPais.CurrentRow.Cells["nombre"].Value.ToString();
            P.NumeroClubs = int.Parse(DgvMenuPais.CurrentRow.Cells["numeroClubs"].Value.ToString());
            P.Representa = int.Parse(DgvMenuPais.CurrentRow.Cells["representa"].Value.ToString());
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void DgvMenuPais_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
