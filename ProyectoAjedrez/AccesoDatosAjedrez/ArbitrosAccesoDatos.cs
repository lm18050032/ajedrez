﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesAjedrez;
namespace AccesoDatosAjedrez
{
    public class ArbitrosAccesoDatos
    {
        ConexionAccesoDatos _conexion;
        public ArbitrosAccesoDatos()
        {
            try
            {
                _conexion = new ConexionAccesoDatos("localhost", "root", "root", "ajedrez", 3306);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);

            }
        }
        public void InsertarArbitro(Arbitro arbitro)
        {
            try
            {
                string Consulta = string.Format("insert into arbitro values('{0}')", arbitro.IdArbitro);
                _conexion.EjecutarConsulta(Consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló el insertar datos en la tabla arbitro " + ex.Message);
            }
        }
        public void EliminarArbitro(string id)
        {
            try
            {
                string Consulta = string.Format("delete from arbitro where IdArbitro = '{0}'", id);
                _conexion.EjecutarConsulta(Consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló el eliminar arbitro " + ex.Message);
            }
        }
        //metodo para datagrid
        public List<VDatosArbitro> ObtenerArbitro(string patronbusq)
        {
            var ListavDatosArbitro = new List<VDatosArbitro>();
            var ds = new DataSet();
            string consulta = string.Format("select * from vdatosarbitro where IdArbitro like '%{0}%'", patronbusq);
            ds = _conexion.ObtenerDatos(consulta, "vdatosarbitro");

            var dt = new DataTable();
            dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var vistadatosarbitro = new VDatosArbitro
                {
                    Idarbitro= int.Parse(row["IdArbitro"].ToString()),
                    Nombre = row["nombre"].ToString(),
                    Direccion = row["direccion"].ToString(),
                    Fkidpais= int.Parse(row["fkidpais"].ToString())
                };
                ListavDatosArbitro.Add(vistadatosarbitro);
                
           }
            return ListavDatosArbitro;
        }
        //metodo para combobox
        public List<ArbitroExtrid> ObtenerIdArbitros()
        {
            var Listaarbitros = new List<ArbitroExtrid>();
            var ds = new DataSet();
            string Consulta = "select IdArbitro as 'IdArbitro', nombre as 'nombre' from vdatosarbitro";
            _conexion.ObtenerDatos(Consulta, "vdatosarbitro");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var idarbitros = new ArbitroExtrid
                {
                    Idarbitro = int.Parse(row["IdArbitro"].ToString()),
                    Nombre = row["nombre"].ToString()                
                };
                Listaarbitros.Add(idarbitros);
            }
            return Listaarbitros;
        }
    }
}
