﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesAjedrez;

namespace AccesoDatosAjedrez
{
    
   public class PartidasAccesoDatos
    {
        ConexionAccesoDatos _conexion;
        public PartidasAccesoDatos()
        {
            try
            {
                _conexion = new ConexionAccesoDatos("localhost", "root", "root", "ajedrez", 3306);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);

            }
        }
        public void InsertarPartida(Partidas p)
        {
            try
            {
                string Consulta = string.Format("insert into partidas values('{0}', '{1}', '{2}', '{3}', '{4}'," +
                    " '{5}', '{6}', '{7}', '{8}', '{9}')", p.IdPartidas, p.Jornada, p.FkIdJugador1, p.Color1, p.FkIdJugador2, p.Color2, p.FkIdArbitro, p.FkIdSala, p.FkIdHotel, p.Entradas);
                _conexion.EjecutarConsulta(Consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló insertar datos en tabla partidas " + ex.Message);
            }
        }
        public void EliminarPartida(string idpartida)
        {
            try
            {
                string Consulta = string.Format("delete from partidas where idPartidas = '{0}'", idpartida);
                _conexion.EjecutarConsulta(Consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló eliminar datos en tabla partidas " + ex.Message);
            }
        }
        public void ActualizarPartida(Partidas p)
        {
            try
            {
                string Consulta = string.Format("update partidas set Jornada = '{0}', FkIdJugador1 = '{1}', Color1 = '{2}'," +
                    "FkIdJugador2 = '{3}', Color2 = '{4}', FkIdArbitro = '{5}', FkIdSala = '{6}', FkIdHotel = '{7}', Entradas = '{8}'" +
                    "where idPartidas = '{9}'", p.Jornada, p.FkIdJugador1, p.Color1, p.FkIdJugador2, p.Color2, p.FkIdArbitro, p.FkIdSala, p.FkIdHotel, p.IdPartidas);
                _conexion.EjecutarConsulta(Consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló actualizar datos en tabla partidas " + ex.Message);
            }
        }
        //metodo mostrar en datagrid
        public List<VDatosPartidas> ObtenerPartidas(string patronbusq)
        {
            var ListaPartidas = new List<VDatosPartidas>();
            var ds = new DataSet();
            string Consulta = string.Format("select * from vdatospartidas where codpartida like '%{0}%'", patronbusq);
            ds = _conexion.ObtenerDatos(Consulta, "vdatospartidas");

            var dt = new DataTable();
            dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var Datospartidas = new VDatosPartidas
                {
                    Codpartida = int.Parse(row["codpartida"].ToString()),
                    Jornada= row["Jornada"].ToString(),
                    Jugador1 = row["Jugador1"].ToString(),
                    Color1 = row["Color1"].ToString(),
                    Jugador2 = row["Jugador2"].ToString(),
                    Color2 = row["Color2"].ToString(),
                    Arbitro= row["Arbitro"].ToString(),
                    Fkidsala = int.Parse(row["FkIdSala"].ToString()),
                    Hotel = row["Hotel"].ToString(),
                    Entradas= int.Parse(row["Entradas"].ToString())
                };
                ListaPartidas.Add(Datospartidas);
            }
            return ListaPartidas;
        }

        public List<ParticipantesExtId> ObtenerIdJug()
        {
            var ListaParticipante = new List<ParticipantesExtId>();
            var ds = new DataSet();
            string consulta = "select p.nSocio as 'nsocio', p.Nombre as 'nombre' from participantes p, jugador j where p.nSocio=j.IdJjugador";
            ds = _conexion.ObtenerDatos(consulta, "participantes");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var participantes = new ParticipantesExtId
                {
                    Idparticipante = int.Parse(row["nSocio"].ToString()),
                    Nombre = row["Nombre"].ToString()
                };
                ListaParticipante.Add(participantes);
            }
            return ListaParticipante;
        }
        public List<ParticipantesExtId> ObtenerIdArb()
        {
            var ListaParticipante = new List<ParticipantesExtId>();
            var ds = new DataSet();
            string consulta = "select arbitro.IdArbitro as 'nSocio', participantes.Nombre as 'Nombre' from participantes, arbitro where participantes.nSocio=arbitro.IdArbitro";
            ds = _conexion.ObtenerDatos(consulta, "participantes");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var participantes = new ParticipantesExtId
                {
                    Idparticipante = int.Parse(row["nSocio"].ToString()),
                    Nombre = row["Nombre"].ToString()
                };
                ListaParticipante.Add(participantes);
            }
            return ListaParticipante;
        }
        public List<HotelExtrId> ObtenerIdHot()
        {
            var ListaHotel = new List<HotelExtrId>();
            var ds = new DataSet();
            string consulta = "select idHotel as 'idhotel', nombre as 'nombre' from hotel";
            ds = _conexion.ObtenerDatos(consulta, "hotel");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var hotelito = new HotelExtrId
                {
                    Idhotel = int.Parse(row["idhotel"].ToString()),
                    Nombre = row["nombre"].ToString()
                };
                ListaHotel.Add(hotelito);
            }
            return ListaHotel;
        }
        public List<Sala> ObtenerIdSal()
        {
            var ListaSala = new List<Sala>();
            var ds = new DataSet();
            string consulta = "select idSala as 'idSala' from sala";
            ds = _conexion.ObtenerDatos(consulta, "sala");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var salita = new Sala
                {
                    IdSala = int.Parse(row["idSala"].ToString())
                    
                };
                ListaSala.Add(salita);
            }
            return ListaSala;
        }

    }
}
