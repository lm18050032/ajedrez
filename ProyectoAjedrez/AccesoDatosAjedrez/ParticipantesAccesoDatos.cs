﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesAjedrez;

namespace AccesoDatosAjedrez
{
   public class ParticipantesAccesoDatos
    {
        ConexionAccesoDatos _conexion;
        public ParticipantesAccesoDatos()
        {
            try
            {
                _conexion = new ConexionAccesoDatos("localhost", "root", "root", "ajedrez", 3306);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);

            }
        }
        public void InsertarParticipantes(Participantes part)
        {
            try
            {
                string consulta = string.Format("insert into participantes values('{0}', '{1}', '{2}', '{3}')", part.IdParticipante, part.Nombre, part.Direccion, part.FkIdPais);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la consulta de guardado" + ex.Message);
            }
        }
        public void EliminarParticipante(string idParticipante)
        {
            try
            {
                string consulta = string.Format("delete from participantes where nSocio = '{0}'", idParticipante);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la consulta de eliminar participante" + ex.Message);
            }
           
        }
        public void ActualizarParticipante(Participantes p)
        {
            try
            {
                string consulta = string.Format("update participantes set Nombre = '{0}', Direccion = '{1}', FkIdPais = '{2}' where nsocio= '{3}'", p.Nombre, p.Direccion, p.FkIdPais, p.IdParticipante);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la consulta de actualizar participante" + ex.Message);
            }
        }
        //metodo para dataGrid
        public List<Participantes> ObtenerParticipantes( string patronbusqueda)
        {
            var ListaParticipantes = new List<Participantes>();
            var ds = new DataSet();
            string Consulta = string.Format("select * from participantes where nsocio like '%{0}%'", patronbusqueda);
            ds = _conexion.ObtenerDatos(Consulta, "participantes");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var participantes = new Participantes
                {
                    IdParticipante = int.Parse(row["nsocio"].ToString()),
                    Nombre = row["nombre"].ToString(),
                    Direccion = row["direccion"].ToString(),
                    FkIdPais= int.Parse(row["Fkidpais"].ToString())
                };
                ListaParticipantes.Add(participantes);
            }
            return ListaParticipantes;
            }
        //metodo para combobox
        public List<ParticipantesExtId> ObtenerIdParticip()
        {
            var ListaParticipante = new List<ParticipantesExtId>();
            var ds = new DataSet();
            string consulta = "select nSocio as 'nsocio', Nombre as 'nombre' from participantes";
            ds = _conexion.ObtenerDatos(consulta, "participantes");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var participantes = new ParticipantesExtId
                {
                    Idparticipante = int.Parse(row["nSocio"].ToString()),
                    Nombre = row["Nombre"].ToString()
                };
                ListaParticipante.Add(participantes);
            }
            return ListaParticipante;
            }
    }

}
