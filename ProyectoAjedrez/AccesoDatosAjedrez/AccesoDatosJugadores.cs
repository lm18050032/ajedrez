﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesAjedrez;
namespace AccesoDatosAjedrez
{
    public class AccesoDatosJugadores
    {
        ConexionAccesoDatos _Conexion;
        public AccesoDatosJugadores()
        {
            try
            {
                _Conexion = new ConexionAccesoDatos("localhost", "root", "root", "ajedrez", 3306);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);

            }
        }
        public void InsertarJugadores(Jugador jugador)
        {
            try
            {
                string consulta = string.Format("insert into jugador values('{0}', '{1}')", jugador.IdJugador, jugador.Nivel);
                _Conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló el guardado de jugadores" + ex.Message);
            }
        }
        public void EliminarJugador(string idjugador)
        {
            try
            {
                string consulta = string.Format("delete from jugador where IdJjugador = '{0}'", idjugador);
                _Conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló eliminar jugadores" + ex.Message);

            }
           
        }
        public void Actualizar(Jugador jugador)
        {
            try
            {
                string consulta = string.Format("update jugador set nivel = '{0}' where IdJjugador = '{1}'", jugador.Nivel, jugador.IdJugador);
                _Conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló actualizar jugador" + ex.Message);
            }
            
        }
        //metodo para datagrid
        public List<VDatosJugadores> ObtenerJugadores(string patronbusq)
        {
            var Vdatosjugadores = new List<VDatosJugadores>();
            var ds = new DataSet();
            string consulta = string.Format("select * from vdatosjugadores where IdJjugador like '%{0}%'", patronbusq);
            ds = _Conexion.ObtenerDatos(consulta, "vdatosjugadores");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var vdatosj = new VDatosJugadores
                {
                    Idjugador= int.Parse(row["IdJjugador"].ToString()),
                    Nombre= row["nombre"].ToString(),
                    Direccion = row["direccion"].ToString(),
                    Fkidpais= int.Parse(row["fkidpais"].ToString()),
                    Nivel= int.Parse(row["nivel"].ToString())
                };
                Vdatosjugadores.Add(vdatosj);
            }
                return Vdatosjugadores;
        }
                //metodo para combobox
        public List<JugadorExtId> ObtenderId()
        {
            var ListaJugadores = new List<JugadorExtId>();
            var ds = new DataSet();
            string Consulta = "select IdJjugador as 'IdJjugador', nombre as 'nombre' from vdatosjugadores";
            ds = _Conexion.ObtenerDatos(Consulta, "vdatosjugadores");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var jugadores = new JugadorExtId
                {
                    Idjugador = int.Parse(row["IdJjugador"].ToString()),
                    Nombre = row["nombre"].ToString()
                     
                };
                ListaJugadores.Add(jugadores);
            }
            return ListaJugadores;
            }
    }
}

