﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesAjedrez;

namespace AccesoDatosAjedrez
{
   public class CampeonatosAccesoDatos
    {
        ConexionAccesoDatos _Conexion;
        public CampeonatosAccesoDatos()
        {
            try
            {
                _Conexion = new ConexionAccesoDatos("localhost", "root", "root", "ajedrez", 3306);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);

            }
        }
        public void InsertarCampeonatos(Campeonato camp)
        {
            try
            {
                string Consulta = string.Format("insert into campeonato values('{0}', '{1}', '{2}', '{3}')", camp.IdCampeonato, camp.Nombre, camp.Tipo, camp.FkIdParticipante);
                _Conexion.EjecutarConsulta(Consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló insertar datos en campeonatos " + ex.Message);

            }
        }
        public void EliminarCampeonato(int idcampeonato)
        {
            try
            {
                string Consulta = string.Format("delete from campeonato where IdCampeonato ='{0}'", idcampeonato);
                _Conexion.EjecutarConsulta(Consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló eliminar datos en campeonatos " + ex.Message);
            }
        }
        public void ActualizarCampeonato(Campeonato camp)
        {
            try
            {
                string Consulta = string.Format("update campeonato set nombre = '{0}', Tipo = '{1}', FkIdparticipante = '{2}' " +
                    "where IdCampeonato = '{3}'", camp.Nombre, camp.Tipo, camp.FkIdParticipante, camp.IdCampeonato);
                _Conexion.EjecutarConsulta(Consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló actualizar datos de campeonato " + ex.Message);
            }
        }
        public List<Campeonato> ObtenerCampeonato(string patronConsulta)
        {
            var ListaCampeonatos = new List<Campeonato>();
            var ds = new DataSet();
            string Consulta = string.Format("select * from campeonato where IdCampeonato like '%{0}%'", patronConsulta);
            ds = _Conexion.ObtenerDatos(Consulta, "campeonato");

            var dt = new DataTable();
            dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var campeonato = new Campeonato
                {
                   IdCampeonato = int.Parse(row["IdCampeonato"].ToString()),
                    Nombre = row["nombre"].ToString(),
                    Tipo= row["Tipo"].ToString(),
                    FkIdParticipante= int.Parse(row["FkIdparticipante"].ToString())
                };
                ListaCampeonatos.Add(campeonato);
            }
            return ListaCampeonatos;
        }
        //public List<ParticipantesExtId> ExtraerIdPart()
        //{
        //    var ListaParticipantes = new List<ParticipantesExtId>();
        //    var ds = new DataSet();
        //    string Consulta = "select nsocio as 'nsocio', nombre as 'nombre' from participantes";
        //    ds = _Conexion.ObtenerDatos(Consulta, "participante");

        //    var dt = new DataTable();
        //    dt = ds.Tables[0];

        //    foreach (DataRow row in dt.Rows)
        //    {

        //        var participantes = new ParticipantesExtId
        //        {
        //            Idparticipante = int.Parse(row["nsocio"].ToString()),
        //            Nombre = row["nombre"].ToString(),
        //        };
        //        ListaParticipantes.Add(participantes);
        //    }
        //    return ListaParticipantes;
        //}
    }
}
