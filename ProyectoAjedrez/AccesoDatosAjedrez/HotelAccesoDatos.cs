﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesAjedrez;

namespace AccesoDatosAjedrez
{
   public class HotelAccesoDatos
    {
        ConexionAccesoDatos _conexion;
        public HotelAccesoDatos()
        {
            try
            {
                _conexion = new ConexionAccesoDatos("localhost", "root", "root", "ajedrez", 3306);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);

            }
        }
        public void InsertarHotel(Hotel hotel)
        {
            try
            {
                string Consulta = string.Format("insert into hotel values('{0}', '{1}', '{2}', '{3}')", hotel.IdHotel,
                    hotel.Nombre, hotel.Direccion, hotel.Telefono);
                _conexion.EjecutarConsulta(Consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló insertar datos en hotel " + ex.Message);
            }
        }
        public void EliminarHotel(int idhotel)
        {
            try
            {
                string Consulta = string.Format("delete from hotel where idHotel = '{0}'", idhotel);
                _conexion.EjecutarConsulta(Consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló eliminar datos en hotel " + ex.Message);
            }
        }
        public void ActualizarHotel(Hotel hotel)
        {
            try
            {
                string Consulta = string.Format("update hotel set nombre = '{0}', Direccion = '{1}', Tel = '{2}' where idHotel  = '{3}'", hotel.Nombre,
                    hotel.Direccion, hotel.Telefono, hotel.IdHotel);
                _conexion.EjecutarConsulta(Consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló actualizar datos en hotel " + ex.Message);
            }
        }
        //metodo para datagrid
        public List<Hotel> ObtenerHoteles(string patronConsulta)
        {
            var ListaHoteles = new List<Hotel>();
            var ds = new DataSet();
            string Consulta = string.Format("select * from hotel where idHotel like '%{0}%'", patronConsulta);
            ds = _conexion.ObtenerDatos(Consulta, "hotel");

            var dt = new DataTable();
            dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var hoteles = new Hotel
                {
                    IdHotel = int.Parse(row["idHotel"].ToString()),
                    Nombre= row["nombre"].ToString(),
                    Direccion= row["Direccion"].ToString(),
                    Telefono= row["Tel"].ToString()
                };
                ListaHoteles.Add(hoteles);
            }
            return ListaHoteles;
            }
        //metodo para combobox
        public List<HotelExtrId> ExtraerIdHotel()
        {
            var ListaHoteles = new List<HotelExtrId>();
            var ds = new DataSet();
            string Consulta = "select idhotel as 'idhotel', nombre as 'nombre' from hotel";
            ds = _conexion.ObtenerDatos(Consulta, "hotel");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var idhoteles = new HotelExtrId
                {
                    Idhotel= int.Parse(row["idHotel"].ToString()),
                    Nombre= row["nombre"].ToString()
                };
                ListaHoteles.Add(idhoteles);
            }
            return ListaHoteles;
            }
    }
}
