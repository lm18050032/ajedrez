﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesAjedrez;

namespace AccesoDatosAjedrez
{
    public class ReservacionesAccesoDatos
    {
        ConexionAccesoDatos _conexion;
        public ReservacionesAccesoDatos()
        {
            try
            {
                _conexion = new ConexionAccesoDatos("localhost", "root", "root", "ajedrez", 3306);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);

            }
        }
        public void InsertarReservaciones(Reserva r)
        {
            try
            {
                string Consulta = string.Format("insert into reserva values('{0}', '{1}', '{2}', '{3}', '{4}')",
                    r.IdReserva, r.FechaEntrada, r.FechaSalida, r.FkIdHotel, r.FkIdParticipantes);
                _conexion.EjecutarConsulta(Consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló el registro de reservaciones" + ex.Message);
            }
        }
        public void EliminarReservacion(int idreserva)
        {
            try
            {
                string Consulta = string.Format("delete from reserva where idReserva = '{0}'", idreserva);
                _conexion.EjecutarConsulta(Consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló eliminar la reservacion" + ex.Message);
            }
        }
        public void ActualizarReservacion(Reserva r)
        {
            try
            {
                string Consulta = string.Format("update reserva set FechaEntrada = '{0}', FechaSalida = '{1}', FkidHotel = '{2}', FkIdParticipante = '{3}'" +
                    " where idReserva = '{4}'", r.FechaEntrada, r.FechaSalida, r.FkIdHotel, r.FkIdParticipantes, r.IdReserva);
                _conexion.EjecutarConsulta(Consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló actualizar la reservacion" + ex.Message);
            }
        }
        public List<Reserva> ObtenerReservas(string patronbusq)
        {
            var ListaReservaciones = new List<Reserva>();
            var ds = new DataSet();
            string Consulta = string.Format("select * from reserva where idReserva like '%{0}%'", patronbusq);
            ds= _conexion.ObtenerDatos(Consulta, "reserva");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var reserva = new Reserva
                {
                    IdReserva = int.Parse(row["idReserva"].ToString()),
                    FechaEntrada = row["FechaEntrada"].ToString(),
                    FechaSalida = row["FechaSalida"].ToString(),
                    FkIdHotel = int.Parse(row["FkidHotel"].ToString()),
                    FkIdParticipantes = int.Parse(row["FkIdParticipante"].ToString())
                };
                ListaReservaciones.Add(reserva);
            }
            return ListaReservaciones;
            }
    }
}
