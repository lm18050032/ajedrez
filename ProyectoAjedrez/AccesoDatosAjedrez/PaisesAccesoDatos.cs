﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesAjedrez;

namespace AccesoDatosAjedrez
{
    
    public class PaisesAccesoDatos
    {
        ConexionAccesoDatos _Conexion;
        public PaisesAccesoDatos()
        {
            try
            {
                _Conexion = new ConexionAccesoDatos("localhost", "root", "root", "ajedrez", 3306);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);

            }
        }
        public void InsertarPaises(Pais pais)
        {
            try
            {
                string consulta = string.Format("insert into pais values('{0}', '{1}','{2}', '{3}')", pais.IdPais, pais.Nombre, pais.NumeroClubs, pais.Representa);
                _Conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la consulta de guardado" + ex.Message);
            }
        }
        public void EliminarPais(int idpais)
        {
            try
            {
                string consulta = string.Format("delete from pais where idPais = '{0}'", idpais);
                _Conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la consulta para eliminar pais" + ex.Message);
            }
        }
        public void ActualizarPais(Pais p)
        {
            try
            {
                string consulta = string.Format("update pais set nombre= '{0}', numeroClubs = '{1}', representa = '{2}' where idpais= '{3}'", p.Nombre, p.NumeroClubs,p.Representa, p.IdPais);
                _Conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la consulta para actualiza datos de pais" + ex.Message);
            }
        }
        public List<Pais> ObtenerPaises(string patroConsulta)
        {
            var ListaPaises = new List<Pais>();
            var ds = new DataSet();
            string consulta = string.Format("select * from pais where idpais like '%{0}%'", patroConsulta);
                ds= _Conexion.ObtenerDatos(consulta, "pais");

            var dt = new DataTable();
            dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var pais = new Pais
                {
                    IdPais = int.Parse(row["idpais"].ToString()),
                    Nombre = row["nombre"].ToString(),
                    NumeroClubs = int.Parse(row["numeroClubs"].ToString()),
                   Representa = int.Parse(row["representa"].ToString())
                };

                ListaPaises.Add(pais);
 
                }
            return ListaPaises;
        }

        public List<PaisesExId> ExtraerId()
        {
            var ListaPaises = new List<PaisesExId>();
            var ds = new DataSet();
            string Consulta = "select idpais as 'idpais', nombre as 'nombre' from pais";
            ds = _Conexion.ObtenerDatos(Consulta, "pais");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                
                var paises = new PaisesExId
                {
                    Idpais = int.Parse(row["idpais"].ToString()),
                    Nombre = row["nombre"].ToString(),
                };
                ListaPaises.Add(paises);
            }
            return ListaPaises;
        }
    }
}
