﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesAjedrez;

namespace AccesoDatosAjedrez
{
    public class MovimientoAccesoDatos
    {
        ConexionAccesoDatos _conexion;
        public MovimientoAccesoDatos()
        {
            try
            {
                _conexion = new ConexionAccesoDatos("localhost", "root", "root", "ajedrez", 3306);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);

            }
        }
        public void InsertarMovimientos(Movimiento mov)
        {
            try
            {
                string Consulta = string.Format("insert into movimiento values('{0}', '{1}', '{2}', '{3}')",
                    mov.IdPartidas, mov.IdMovimiento, mov.Movimientos, mov.Comentario);
                _conexion.EjecutarConsulta(Consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló insertar datos en tabla movimiento " + ex.Message);
            }
        }
        public void EliminarMovimiento(string idmovimiento)
        {
            try
            {
                string Consulta = string.Format("delete from movimiento where IdMovimiento = '{0}'", idmovimiento);
                _conexion.EjecutarConsulta(Consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló eliminar datos en tabla movimiento " + ex.Message);
            }
        }
        public void ActualizarMovimiento(Movimiento m)
        {
            try
            {
                string Consulta = string.Format("update movimiento set idPartidas = '{0}', movimiento = '{1}', Comentario='{2}' where " +
                    "IdMovimiento = '{3}'", m.IdPartidas, m.Movimientos, m.Comentario, m.IdMovimiento);
                _conexion.EjecutarConsulta(Consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló actualizar datos en tabla movimiento " + ex.Message);
            }
        }
        public List<Movimiento> Obtenermovimientos(string patronbusq)
        {
            var ListaMov = new List<Movimiento>();
            var ds = new DataSet();
            string Consulta = string.Format("select * from movimiento where IdMovimiento like '%{0}%'", patronbusq);
            ds = _conexion.ObtenerDatos(Consulta, "movimiento");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var mov = new Movimiento
                {
                    IdPartidas = int.Parse(row["idPartidas"].ToString()),
                    IdMovimiento=int.Parse(row["IdMovimiento"].ToString()),
                    Movimientos= row["movimiento"].ToString(),
                    Comentario= row["Comentario"].ToString()
                };
                ListaMov.Add(mov);
            }

            return ListaMov;
        }


        public List<Partidas> ObtenerIdPartida()
        {
            var ListaSala = new List<Partidas>();
            var ds = new DataSet();
            string consulta = "select idPartidas as 'idPartidas' from partidas";
            ds = _conexion.ObtenerDatos(consulta, "partidas");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var partidita = new Partidas
                {
                    IdPartidas = int.Parse(row["idPartidas"].ToString())

                };
                ListaSala.Add(partidita);
            }
            return ListaSala;
        }



    }
}
