﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesAjedrez;

namespace AccesoDatosAjedrez
{
    public class SalasAccesoDatos
    {
        ConexionAccesoDatos _conexion;
        public SalasAccesoDatos()
        {
            try
            {
                _conexion = new ConexionAccesoDatos("localhost", "root", "root", "ajedrez", 3306);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la conexión " + ex.Message);

            }
        }
        public void InsertarSalas(Sala sala)
        {
            try
            {
                string Consulta = string.Format("insert into sala values('{0}', '{1}', '{2}', '{3}')", sala.IdHotel, sala.IdSala, sala.Capacidad, sala.Medios);
                _conexion.EjecutarConsulta(Consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló insertar datos de salas " + ex.Message);
            }
        }
        public void EliminarSala(int idsala)
        {
            try
            {
                string Consulta = string.Format("delete from sala where idSala = '{0}'", idsala);
                _conexion.EjecutarConsulta(Consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló eliminar datos de salas " + ex.Message);
            }
        }
        public void ActualizarSala(Sala sala)
        {
            try
            {
                string Consulta = string.Format("update sala set IdHotel = '{0}', Capacidad ='{1}', Medios= '{2}' " +
                    "where idSala = '{3}'", sala.IdHotel, sala.Capacidad, sala.Medios, sala.IdSala);
                _conexion.EjecutarConsulta(Consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló actualizar datos de salas " + ex.Message);
            }
        }
        public List<Sala> ObtenerSalas(string patronbusq)
        {
            var ListaSalas = new List<Sala>();
            var ds = new DataSet();

            string Consulta = string.Format("select * from sala where idSala like '%{0}%'", patronbusq);
            ds = _conexion.ObtenerDatos(Consulta, "sala");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var salas = new Sala
                {
                    IdHotel = int.Parse(row["IdHotel"].ToString()),
                    IdSala = int.Parse(row["idSala"].ToString()),
                    Capacidad = int.Parse(row["Capacidad"].ToString()),
                    Medios = row["Medios"].ToString()
                };
                ListaSalas.Add(salas);
            }
            return ListaSalas;
        }
    }
}
