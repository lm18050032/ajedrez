﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
   public class VDatosPartidas
    {
        private int _codpartida;
        private string _jornada;
        private string _jugador1;
        private string _color1;
        private string _jugador2;
        private string _color2;
        private string _arbitro;
        private int _fkidsala;
        private string _Hotel;
        private int _entradas;

        public int Codpartida { get => _codpartida; set => _codpartida = value; }
        public string Jornada { get => _jornada; set => _jornada = value; }
        public string Jugador1 { get => _jugador1; set => _jugador1 = value; }
        public string Color1 { get => _color1; set => _color1 = value; }
        public string Jugador2 { get => _jugador2; set => _jugador2 = value; }
        public string Color2 { get => _color2; set => _color2 = value; }
        public string Arbitro { get => _arbitro; set => _arbitro = value; }
        public int Fkidsala { get => _fkidsala; set => _fkidsala = value; }
        public string Hotel { get => _Hotel; set => _Hotel = value; }
        public int Entradas { get => _entradas; set => _entradas = value; }
    }
}
