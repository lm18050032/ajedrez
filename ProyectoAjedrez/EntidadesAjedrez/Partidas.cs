﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
   public class Partidas
    {

        private int _idpartidas;
        private string _jornada;
        private int _fkidjugador1;
        private string _color1;
        private int _fkidjugador2;
        private string _color2;
        private int _idarbitro;
        private int _fkidhotel;
        private int _fkidsala;
        private int _entradas;

        public int IdPartidas { get => _idpartidas; set => _idpartidas = value; }
        public string Jornada { get => _jornada; set => _jornada = value; }
        public int FkIdJugador1 { get => _fkidjugador1; set => _fkidjugador1 = value; }
        public string   Color1 { get => _color1; set => _color1 = value; }
        public int  FkIdJugador2 { get => _fkidjugador2; set => _fkidjugador2 = value; }
        public string Color2 { get => _color2; set => _color2 = value; }
        public int FkIdArbitro { get => _idarbitro; set => _idarbitro = value; }
        public int FkIdHotel  { get => _fkidhotel; set => _fkidhotel = value; }

        public int FkIdSala { get => _fkidsala; set => _fkidsala = value; }
        public int Entradas { get => _entradas; set => _entradas = value; }


    }
}
