﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
  public  class Pais
    {
        private int _idpais;
        private string _nombre;
        private int _numeroclubs;
        private int _representa;
        public int IdPais { get => _idpais; set => _idpais = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public int NumeroClubs { get => _numeroclubs; set => _numeroclubs = value; }
        public int Representa { get => _representa; set => _representa = value; }
    }
}
