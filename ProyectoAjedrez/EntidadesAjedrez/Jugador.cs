﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
   public  class Jugador
    {
        private int _idjugador;
        private int _nivel;

        public int IdJugador { get => _idjugador; set => _idjugador = value; }
        public int  Nivel { get => _nivel; set => _nivel = value; }
    }
}
