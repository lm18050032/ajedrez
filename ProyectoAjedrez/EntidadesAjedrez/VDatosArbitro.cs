﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
   public class VDatosArbitro
    {
        private int _idarbitro;
        private string _nombre;
        private string _direccion;
        private int fkidpais;

        public int Idarbitro { get => _idarbitro; set => _idarbitro = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string Direccion { get => _direccion; set => _direccion = value; }
        public int Fkidpais { get => fkidpais; set => fkidpais = value; }
    }
}
