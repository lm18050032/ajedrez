﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
   public class Sala
    {

        private int _idHotel;
        private int _idsala;
        private int _capacidad;
        private string _medios;

        public int IdHotel { get => _idHotel; set => _idHotel = value; }
        public int IdSala { get => _idsala; set => _idsala = value; }
        public int Capacidad { get =>_capacidad; set => _capacidad = value; }
        public string Medios { get => _medios; set => _medios = value; }

    }
}
