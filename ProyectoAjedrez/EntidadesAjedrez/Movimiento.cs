﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
   public  class Movimiento
    {
        private int _idpartidas;
        private int _idmovimiento;
        private string _movimiento;
        private string _comentario;


        public int IdPartidas { get => _idpartidas; set => _idpartidas = value; }
        public int IdMovimiento { get => _idmovimiento; set => _idmovimiento = value; }
        public string Movimientos { get => _movimiento; set => _movimiento = value; }
        public string Comentario { get => _comentario; set => _comentario = value; }
    }
}
