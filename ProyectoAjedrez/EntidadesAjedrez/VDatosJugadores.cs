﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
    public class VDatosJugadores
    {
        private int _Idjugador;
        private string _nombre;
        private string _direccion;
        private int _fkidpais;
        private int _nivel;

        public int Idjugador { get => _Idjugador; set => _Idjugador = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string Direccion { get => _direccion; set => _direccion = value; }
        public int Fkidpais { get => _fkidpais; set => _fkidpais = value; }
        public int Nivel { get => _nivel; set => _nivel = value; }
    }
}
