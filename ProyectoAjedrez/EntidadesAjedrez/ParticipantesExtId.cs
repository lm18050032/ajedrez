﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
    public class ParticipantesExtId
    {
        private int _Idparticipante;
        private string _Nombre;

        public int Idparticipante { get => _Idparticipante; set => _Idparticipante = value; }
        public string Nombre { get => _Nombre; set => _Nombre = value; }
    }
}
