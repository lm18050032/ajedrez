﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
   public class JugadorExtId
    {
        
        private string _nombre;
        private int _idjugador;

        public string Nombre { get => _nombre; set => _nombre = value; }
        public int Idjugador { get => _idjugador; set => _idjugador = value; }
    }
}
