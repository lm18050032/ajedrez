﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
   public class Participantes
    {

        private int _idparticipante;
        private string _nombre;
        private string _direccion;
        private int _fkidpais;

        public int IdParticipante { get => _idparticipante; set => _idparticipante = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string Direccion { get => _direccion; set => _direccion = value; }
        public int FkIdPais { get => _fkidpais; set => _fkidpais = value; }

    }
}
