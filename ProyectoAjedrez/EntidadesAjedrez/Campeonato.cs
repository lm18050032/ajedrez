﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
    public class Campeonato
    {

        private int _idCampeonato;
        private string _nombre;
        private string _tipo;
        private int _fkidparticipante;

        public int IdCampeonato { get => _idCampeonato; set => _idCampeonato = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string Tipo { get => _tipo; set => _tipo = value; }
        public int FkIdParticipante { get => _fkidparticipante; set => _fkidparticipante = value; }
    }
}
