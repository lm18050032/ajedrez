﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
   public  class Reserva
    {

        private int _idreserva;
        private string _fechaentrda;
        private string _fechasalida;
        private int _fkidhotel;
        private int _fkidparticipantes;

        public int IdReserva { get => _idreserva; set => _idreserva = value; }
        public string FechaEntrada { get => _fechaentrda; set => _fechaentrda = value; }
        public string FechaSalida { get => _fechasalida; set => _fechasalida = value; }
        public int FkIdHotel { get => _fkidhotel; set => _fkidhotel = value; }
        public int FkIdParticipantes { get => _fkidparticipantes; set => _fkidparticipantes = value; }
    }

}
