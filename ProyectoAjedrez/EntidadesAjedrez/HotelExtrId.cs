﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
    public class HotelExtrId
    {
        private int _idhotel;
        private string _nombre;

        public int Idhotel { get => _idhotel; set => _idhotel = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
    }
}
